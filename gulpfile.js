//A2C X-Ray

'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
 
//SASS
gulp.task('sass', function () {
    return gulp.src('*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('./'));
});
//SASS

//JS
gulp.task('js', function() {
    return gulp.src('./a2c.xray.js')
        .pipe(concat('a2c.xray.min.js'))
        .pipe(gulp.dest('./'))
        .pipe(uglify())
        .pipe(gulp.dest('./'));
});
//JS
 
gulp.task('w', function () {
    gulp.watch('*.scss', ['sass']);
    gulp.watch('a2c.xray.js', ['js']);
});