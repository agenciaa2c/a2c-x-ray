<!DOCTYPE html>
<html lang="pt">
<head>
    
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.">

    <title>A2C X-Ray</title>
    
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="a2c.xray.css">

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script src="a2c.xray.min.js"></script>

    <script>

        $(document).ready(function(){

            <?php if( @$_GET['a2c'] == 1 || @$_COOKIE["a2c_xray"] == '1' ): ?>
                <?php setcookie("a2c_xray","1"); ?>
                $(this).activateXRay();
            <?php endif; ?>

            <?php if( @$_GET['a2c'] == 0 ): ?>
                <?php setcookie("a2c_xray","0"); ?>
            <?php endif; ?>
            
        });

    </script>

</head>
<body>

    <div class='container'>

        <div class='row'>
            <div class='col-xs-12'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:100px;'></div>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-11'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:50px;'></div>
            </div>
            <div class='col-xs-1'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:50px;'></div>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-10'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:100px;'></div>
            </div>
            <div class='col-xs-2'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:100px;'></div>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-9'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:50px;'></div>
            </div>
            <div class='col-xs-3'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:50px;'></div>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-8'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:100px;'></div>
            </div>
            <div class='col-xs-4'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:100px;'></div>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-7'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:50px;'></div>
            </div>
            <div class='col-xs-5'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:50px;'></div>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-6'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:100px;'></div>
            </div>
            <div class='col-xs-6'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:100px;'></div>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-3'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:50px;'></div>
            </div>
            <div class='col-xs-3'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:50px;'></div>
            </div>
            <div class='col-xs-3'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:50px;'></div>
            </div>
            <div class='col-xs-3'>
                <div style='width:100%;background:#000;color:#fff;margin-top:20px;height:50px;'></div>
            </div>
        </div>

        
        <div class='row'>
            <div class='col-xs-12'>
                <h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h1>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-6'>
                <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h2>
            </div>
            <div class='col-xs-6'>
                <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h2>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-3'>
                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
            </div>
            <div class='col-xs-3'>
                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
            </div>
            <div class='col-xs-3'>
                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
            </div>
            <div class='col-xs-3'>
                <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h3>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-6'>
                <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
            </div>
            <div class='col-xs-6'>
                <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-2'>
                <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
            </div>
            <div class='col-xs-2'>
                <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
            </div>
            <div class='col-xs-2'>
                <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
            </div>
            <div class='col-xs-2'>
                <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
            </div>
            <div class='col-xs-2'>
                <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
            </div>
            <div class='col-xs-2'>
                <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h5>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-12'>
                <h6>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h6><br>
            </div>
        </div>
    </div>

    <div class='container'>
        <div class='row'>
            <div class='col-xs-6'>
                <a href='#' title='Link com Title'>Link com Title</a><br><br><br>
            </div>
            <div class='col-xs-6'>
                <a href='#'>Link sem Title</a><br><br><br>
            </div>
        </div>
    </div>

    <div class='container'>
        <div class='row'>
            <div class='col-xs-6'>
                <img src='https://via.placeholder.com/200x200' alt='Imagem com alt'><br>Imagem com alt.
            </div>
            <div class='col-xs-6'>
                    <img src='https://via.placeholder.com/200x200'><br>Imagem sem alt.
            </div>
        </div>
    </div>
    
</body>
</html>