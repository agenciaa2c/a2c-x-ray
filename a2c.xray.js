// jQuery A2C X-Ray v1.0.0

(function( $ ) {

    window.inputSavedValue = '';

    function countPreviousBox(){
        $('.a2c_xray_floater-counter-characters').each(function(){
            $(this).text($(this).prev().val().length);
        });
    }
 
    $.fn.activateXRay = function() {

        //---- COLOCA CLASSE PARA VISUALIZAÇÃO SEO

            $('body').addClass('a2c_x_ray_seo');
        
        //---- COLOCA DIV FLUTUANTE PARA VISUALIZAÇÃO DE DADOS

            if( $.cookie('seoFloaterStatus') == '' || $.cookie('seoFloaterStatus') == undefined ){
                $.cookie('seoFloaterStatus', 'opened', { path: '/' });
            }

            $('body').append("<div class='a2c_xray_floater "+ $.cookie('seoFloaterStatus') +"'><div class='a2c_xray_floater-scroll'><svg version='1.1' id='Camada_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px'viewBox='0 0 391.4 391.4' style='enable-background:new 0 0 391.4 391.4;' xml:space='preserve'><style type='text/css'>.st0{fill:#FFFFFF;}</style><g><g><path class='st0' d='M195.7,0C87.6,0,0,87.6,0,195.7c0,108.1,87.6,195.7,195.7,195.7c108.1,0,195.7-87.6,195.7-195.7C391.4,87.6,303.7,0,195.7,0z M226.1,184.2v41.5v11.8c0,1.6-1.3,3-3,3h-10.4c-1.6,0-3-1.3-3-3v-33.7l-20.5,5.6c-3.1,1.1-5.8,3.3-6.2,6.7v21.4c0,1.6-1.3,3-3,3h-10.4c-1.6,0-3-1.3-3-3v-11.8h0v-53c0-4.7,1.4-9.1,4.5-12.7c5.1-5.7,15.1-8.6,25.2-8.6c10.1,0,20.2,2.9,25.2,8.6c3.1,3.6,4.5,8,4.5,12.7V184.2z M296.6,179.7c0,1.8,0,3.5,0,5.4c0,7.7-6.3,15-13.3,17.2c-1.8,0.6-3.6,1.1-5.4,1.6l-1.1,0.3l-16.9,4.6l0,0l-0.2,0.1c-0.2,0.1-0.4,0.2-0.6,0.3c0,0-0.1,0-0.1,0c-0.1,0.1-0.2,0.1-0.4,0.1c-0.1,0.1-0.2,0.1-0.4,0.2l0,0c-0.1,0.1-0.2,0.1-0.3,0.2c-0.3,0.1-0.5,0.3-0.8,0.5c-0.1,0-0.1,0.1-0.2,0.1c-0.2,0.2-0.5,0.3-0.7,0.5c-0.2,0.1-0.4,0.3-0.5,0.5c-0.1,0.1-0.2,0.2-0.3,0.3c-0.2,0.2-0.4,0.5-0.6,0.7c-0.1,0.1-0.1,0.2-0.2,0.2c-0.2,0.2-0.3,0.5-0.4,0.7c0,0.1-0.1,0.1-0.1,0.2c-0.1,0.3-0.3,0.6-0.4,1c0,0.1,0,0.2-0.1,0.2c-0.1,0.4-0.2,0.7-0.2,1.1v8h41.3c0.9,0,1.6,0.7,1.6,1.6v13.2c0,0.9-0.7,1.6-1.6,1.6h-56.1c-0.9,0-1.6-0.7-1.6-1.6v-0.2v-13.1v-9.5V212c0.5-7.3,6.7-13.8,13.3-16.4c1.1-0.4,2.1-0.8,3.2-1.1l6.8-1.9L274,189c3.2-1.1,5.8-3.3,6.3-6.8V175v0c0-0.4,0-0.9-0.1-1.3c0,0,0-0.1,0-0.1c0-0.2-0.1-0.4-0.1-0.6c0-0.1,0-0.1,0-0.2c0-0.2-0.1-0.3-0.1-0.5c0-0.1,0-0.2-0.1-0.3c0-0.1-0.1-0.3-0.1-0.4c0-0.1-0.1-0.2-0.1-0.3c-0.1-0.1-0.1-0.3-0.2-0.4c-0.1-0.1-0.1-0.2-0.2-0.4c-0.1-0.1-0.1-0.2-0.2-0.3c-0.1-0.1-0.2-0.3-0.3-0.4c-0.1-0.1-0.1-0.2-0.2-0.2c-0.2-0.2-0.3-0.4-0.5-0.6c-4.4-4.8-18.2-4.8-22.5,0c-1.6,1.7-2.1,3.8-2.1,6.1v1c0,0.2,0,0.4,0,0.6c0,2.4-1.4,4.4-3.7,5l-10.3,2.7c-1.2,0.3-2.3-0.4-2.3-1.7v-10.3c0-4.7,1.4-9.2,4.5-12.8c3.9-4.5,10.9-7.3,18.5-8.3c1.1-0.1,2.2-0.2,3.3-0.3c0,0,0.1,0,0.1,0c1.1-0.1,2.2-0.1,3.3-0.1c1.1,0,2.3,0.1,3.4,0.1c0.3,0,0.6,0,1,0.1c0.9,0.1,1.9,0.2,2.8,0.3c0.2,0,0.3,0,0.5,0.1c0.9,0.1,1.8,0.3,2.7,0.5c0,0,0,0,0,0c6.2,1.3,11.6,3.8,14.9,7.6c2.8,3.2,4.2,7,4.5,11.1c0,0.5,0.1,1.1,0.1,1.6V179.7z M348.7,169.2c-4.4-4.8-18.2-4.8-22.6,0c-1.6,1.7-2.1,3.8-2.1,6.1v41.2c0,2.2,0.5,4.3,2.1,6c4.5,4.8,18.1,4.8,22.6,0c1.6-1.6,2.1-3.7,2.1-6v-3.1c0-2.4,1.4-4.4,3.8-5l10.4-2.7c1.2-0.3,2.3,0.4,2.3,1.7v11.8c0,4.7-1.4,9.1-4.5,12.7c-10.1,11.5-40.4,11.5-50.6,0c-3.1-3.6-4.5-8-4.5-12.7v-46.7c0-4.7,1.4-9.1,4.5-12.7c5.1-5.8,15.2-8.6,25.3-8.6c10.1,0,20.2,2.9,25.3,8.6c3.1,3.6,4.5,8,4.5,12.7v4.2c0,2.4-1.4,4.3-3.8,5l-10.4,2.7c-1.2,0.3-2.3-0.4-2.3-1.7v-7.4C350.8,173,350.2,170.9,348.7,169.2z'/><path class='st0' d='M185.1,169.4c-1.6,1.7-2.1,3.8-2.1,6.1v8.6c0,0,0,0.1,0,0.1V195l20.5-5.6c3.2-1.1,5.8-3.3,6.3-6.8v-7.1c0-2.3-0.6-4.4-2.1-6.1C203.3,164.6,189.5,164.6,185.1,169.4z'/></g></g></svg></div><div class='a2c_xray_floater-close'></div></div>");

            $('.a2c_xray_floater-scroll').append("<div class='a2c_xray_floater-counter'><div class='a2c_xray_floater-counter-title'>Title</div><input type='text' value='"+ $(document).find('title').text() +"'><div class='a2c_xray_floater-counter-characters'></div></div>");
            $('.a2c_xray_floater-scroll').append("<div class='a2c_xray_floater-counter'><div class='a2c_xray_floater-counter-title'>Description</div><textarea>"+ $(document).find('meta[name=description]').attr('content') +"</textarea><div class='a2c_xray_floater-counter-characters'></div></div>");
            $('.a2c_xray_floater-scroll').append("<div class='a2c_xray_floater-counter h1'><div class='a2c_xray_floater-counter-title'>H1 - <b>"+ $('h1').length +"</b></div></div>");
            $('.a2c_xray_floater-scroll').append("<div class='a2c_xray_floater-counter h2'><div class='a2c_xray_floater-counter-title'>H2 - <b>"+ $('h2').length +"</b></div></div>");
            $('.a2c_xray_floater-scroll').append("<div class='a2c_xray_floater-counter h3'><div class='a2c_xray_floater-counter-title'>H3 - <b>"+ $('h3').length +"</b></div></div>");
            $('.a2c_xray_floater-scroll').append("<div class='a2c_xray_floater-counter h4'><div class='a2c_xray_floater-counter-title'>H4 - <b>"+ $('h4').length +"</b></div></div>");
            $('.a2c_xray_floater-scroll').append("<div class='a2c_xray_floater-counter h5'><div class='a2c_xray_floater-counter-title'>H5 - <b>"+ $('h5').length +"</b></div></div>");
            $('.a2c_xray_floater-scroll').append("<div class='a2c_xray_floater-counter h6'><div class='a2c_xray_floater-counter-title'>H6 - <b>"+ $('h6').length +"</b></div></div>");
            $('.a2c_xray_floater-scroll').append("<div class='a2c_xray_floater-counter h_grid_control'><div class='a2c_xray_floater-counter-title'>Grid Horizontal</div><div class='a2c_xray_floater-counter-switch' data-switch='a2c_x_ray_grid'></div></div>");
            $('.h_grid_control').append("<div class='h_grid_control-field container_width'>Container Width<br><input type='text' value='1170px'></div>");
            $('.h_grid_control').append("<div class='h_grid_control-field col_padding'>Col Padding<br><input type='text' value='15px'></div>");
            $('.a2c_xray_floater-scroll').append("<div class='a2c_xray_floater-counter'><div class='a2c_xray_floater-counter-title'>Grid Vertical</div><div class='a2c_xray_floater-counter-switch' data-switch='a2c_x_ray_vertical_grid'></div></div>");


            countPreviousBox();
        
        //---- COLOCA DIV FLUTUANTE PARA VISUALIZAÇÃO DE DADOS
        
        //---- FECHAR E EXPANDIR SIDEBAR

            $('body').on('click','.a2c_xray_floater-close',function(){
                $('.a2c_xray_floater').toggleClass('closed');
                if( $('.a2c_xray_floater').hasClass('closed') ){
                    $.cookie('seoFloaterStatus', 'closed', { path: '/' });
                }else{
                    $.cookie('seoFloaterStatus', 'opened', { path: '/' });
                }
            });
        
        //---- FECHAR E EXPANDIR SIDEBAR
        
        //---- CONTAGEM DE CARACTERES E COMPORTAMENTO DOS INPUTS

            $('body').on('keyup','input,textarea',function(){
                countPreviousBox();
            });

            $('body').on('focus','input,textarea',function(){
                window.inputSavedValue = $(this).val();
            });

            $('body').on('blur','input,textarea',function(){
                if( $(this).val() == '' ){
                    $(this).val(window.inputSavedValue);
                    countPreviousBox();
                }
            });
        
        //---- CONTAGEM DE CARACTERES E COMPORTAMENTO DOS INPUTS
        
        //---- GRID

            $('body').append('<div class="a2c_xray_vertical_grid"></div>');
            $('body').append('<div class="a2c_xray_grid"><div class="container"><div class="row"></div></div></div>');
            for( var i = 0; i < 12; i++ ){ $('.a2c_xray_grid .container .row').append('<div></div>'); }
            if( $('body').hasClass('a2c_x_ray_grid') ){
                $('.a2c_xray_floater-counter-switch[data-switch=a2c_x_ray_grid]').addClass('active');
            }
            if( $('body').hasClass('a2c_x_ray_vertical_grid') ){
                $('.a2c_xray_floater-counter-switch[data-switch=a2c_x_ray_vertical_grid]').addClass('active');
            }

            
            if( $.cookie('container_width') == '' || $.cookie('container_width') == undefined ){
                $.cookie('container_width',$('.container_width input').val(), { path: '/' } );
            }else{
                $('.container_width input').val($.cookie('container_width'));
                $('.a2c_xray_grid .container').css('width',$('.container_width input').val());
            }
            if( $.cookie('col_padding') == '' || $.cookie('col_padding') == undefined ){
                $.cookie('col_padding',$('.col_padding input').val(), { path: '/' } );
            }else{
                $('.col_padding input').val($.cookie('col_padding'));
                $('.a2c_xray_grid .row div').css('padding-left',$('.col_padding input').val());
                $('.a2c_xray_grid .row div').css('padding-right',$('.col_padding input').val());
            }

            $('body').on('keyup','.container_width input',function(){
                $.cookie('container_width',$('.container_width input').val(), { path: '/' } );
                $('.a2c_xray_grid .container').css('width',$('.container_width input').val());
            });

            $('body').on('keyup','.col_padding input',function(){
                $.cookie('col_padding',$('.col_padding input').val(), { path: '/' } );
                $('.a2c_xray_grid .row div').css('padding-left',$('.col_padding input').val());
                $('.a2c_xray_grid .row div').css('padding-right',$('.col_padding input').val());
            });
        
        //---- GRID
        
        //---- SWITCH

            $('body').on('click','.a2c_xray_floater-counter-switch',function(){

                $('body').toggleClass($(this).data('switch'));
                $(this).toggleClass('active');

                if( $(this).hasClass('active') ){
                    $.cookie($(this).data('switch'), '1', { path: '/' });
                }else{
                    $.cookie($(this).data('switch'), '0', { path: '/' });
                }

            });

            $('.a2c_xray_floater-counter-switch').each(function(){
                if( $.cookie($(this).data('switch')) == '1' ){
                    $(this).addClass('active');
                    $('body').addClass($(this).data('switch'));
                }
            });
        
        //---- SWITCH
        
        //---- HEADING_TAGS

            var totalHeadings = [];
            var currentHeading = [];
            
            totalHeadings['h1'] = $('h1').length,currentHeading['h1'] = 0;
            totalHeadings['h2'] = $('h2').length,currentHeading['h2'] = 0;
            totalHeadings['h3'] = $('h3').length,currentHeading['h3'] = 0;
            totalHeadings['h4'] = $('h4').length,currentHeading['h4'] = 0;
            totalHeadings['h5'] = $('h5').length,currentHeading['h5'] = 0;
            totalHeadings['h6'] = $('h6').length,currentHeading['h6'] = 0;
            
            $('body').on('click','.h1,.h2,.h3,.h4,.h5,.h6',function(){

                var clickedHeading = $(this).attr('class').split(' ')[1];

                console.log(clickedHeading);

                $(clickedHeading).removeClass('active');
                $(clickedHeading + ':eq(' + currentHeading[clickedHeading]+')').addClass('active');

                $('body,html').animate({ 'scrollTop' : $(clickedHeading + ':eq(' + currentHeading[clickedHeading]+')').offset().top - 100 } , 200 );

                currentHeading[clickedHeading]++;

                if( currentHeading[clickedHeading] == totalHeadings[clickedHeading] ){
                    currentHeading[clickedHeading] = 0;
                }

            });
        
        //---- HEADING_TAGS

        //---- FIM

        return this;
 
    };
 
}( jQuery ));